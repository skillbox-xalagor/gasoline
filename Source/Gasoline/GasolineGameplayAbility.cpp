﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "GasolineGameplayAbility.h"

#include "GasolineCharacter.h"

UGasolineGameplayAbility::UGasolineGameplayAbility(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	ReplicationPolicy = EGameplayAbilityReplicationPolicy::ReplicateNo;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
	NetSecurityPolicy = EGameplayAbilityNetSecurityPolicy::ClientOrServer;
}

AGasolineCharacter* UGasolineGameplayAbility::GetGasolineCharacterFromActorInfo() const
{
	return (CurrentActorInfo ? Cast<AGasolineCharacter>(CurrentActorInfo->AvatarActor.Get()) : nullptr);
}
