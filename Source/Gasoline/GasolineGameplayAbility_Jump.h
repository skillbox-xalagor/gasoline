﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GasolineGameplayAbility.h"
#include "GasolineGameplayAbility_Jump.generated.h"

/**
 * Jumping ability
 */
UCLASS()
class GASOLINE_API UGasolineGameplayAbility_Jump : public UGasolineGameplayAbility
{
	GENERATED_BODY()
	
public:
    
    UGasolineGameplayAbility_Jump(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

protected:
    
	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
    
	UFUNCTION(BlueprintCallable, Category = "Ability")
	void CharacterJumpStart();
    
	UFUNCTION(BlueprintCallable, Category = "Ability")
	void CharacterJumpStop();
};
