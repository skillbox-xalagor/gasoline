// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "GasolineAbilitySystemComponent.generated.h"

/**
 * 
 */
UCLASS()
class GASOLINE_API UGasolineAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()
	
};
