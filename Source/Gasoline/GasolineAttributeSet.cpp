// Fill out your copyright notice in the Description page of Project Settings.


#include "GasolineAttributeSet.h"

#include "Net/UnrealNetwork.h"

UGasolineAttributeSet::UGasolineAttributeSet()
{
	
}

void UGasolineAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME_CONDITION_NOTIFY(UGasolineAttributeSet, Health, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UGasolineAttributeSet, MaxHealth, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UGasolineAttributeSet, Energy, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UGasolineAttributeSet, MaxEnergy, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UGasolineAttributeSet, Power, COND_None, REPNOTIFY_Always);
}

void UGasolineAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);

	if (Attribute == GetMaxHealthAttribute())
	{
		AdjustAttributeForMaxChange(Health, MaxHealth, NewValue, GetHealthAttribute());
	}
	else if (Attribute == GetMaxEnergyAttribute())
	{
		AdjustAttributeForMaxChange(Energy, MaxEnergy, NewValue, GetEnergyAttribute());
	}
}

void UGasolineAttributeSet::AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute,
	const FGameplayAttributeData& MaxAttribute, float NewMaxAttributeValue,
	const FGameplayAttribute& AffectedAttributeProperty)
{
	UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();

	const float CurrentMaxAttributeValue = MaxAttribute.GetCurrentValue()
		== 0 ? NewMaxAttributeValue : MaxAttribute.GetCurrentValue();

	if (!FMath::IsNearlyEqual(CurrentMaxAttributeValue, NewMaxAttributeValue) && AbilityComp)
	{
		const float CurrentAffectedAttributeValue = AffectedAttribute.GetCurrentValue();
		float NewCurrentValue = CurrentAffectedAttributeValue * NewMaxAttributeValue / CurrentMaxAttributeValue;

		AbilityComp->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Override, NewCurrentValue);
	}
}

void UGasolineAttributeSet::OnRep_Health(const FGameplayAttributeData& OldHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGasolineAttributeSet, Health, OldHealth);
}

void UGasolineAttributeSet::OnRep_MaxHealth(const FGameplayAttributeData& OldMaxHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGasolineAttributeSet, MaxHealth, OldMaxHealth);
}

void UGasolineAttributeSet::OnRep_Energy(const FGameplayAttributeData& OldEnergy)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGasolineAttributeSet, Energy, OldEnergy);
}

void UGasolineAttributeSet::OnRep_MaxEnergy(const FGameplayAttributeData& OldMaxEnergy)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGasolineAttributeSet, MaxEnergy, OldMaxEnergy);
}

void UGasolineAttributeSet::OnRep_Power(const FGameplayAttributeData& OldPower)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGasolineAttributeSet, Power, OldPower);
}
