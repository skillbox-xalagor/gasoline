﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "GasolineGameInstance.generated.h"

/**
 * GameInstance with optimizations
 */
UCLASS()
class GASOLINE_API UGasolineGameInstance : public UGameInstance
{
	GENERATED_BODY()

	virtual void Init() override;
};
