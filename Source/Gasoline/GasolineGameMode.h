// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GasolineGameMode.generated.h"

UCLASS(minimalapi)
class AGasolineGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGasolineGameMode();
};



