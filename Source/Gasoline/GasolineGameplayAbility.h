﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GasolineCharacter.h"
#include "Abilities/GameplayAbility.h"
#include "Gasoline/Gasoline.h"
#include "GasolineGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class GASOLINE_API UGasolineGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	
	UGasolineGameplayAbility(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	
	UFUNCTION(BlueprintCallable, Category = "Ability")
	AGasolineCharacter* GetGasolineCharacterFromActorInfo() const;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
	EGasolineAbilityInputID AbilityInputID = EGasolineAbilityInputID::None;
};
