// Copyright Epic Games, Inc. All Rights Reserved.

#include "GasolineGameMode.h"
#include "GasolineCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGasolineGameMode::AGasolineGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
