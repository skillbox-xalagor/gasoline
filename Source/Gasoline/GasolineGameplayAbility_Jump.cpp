﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GasolineGameplayAbility_Jump.h"

UGasolineGameplayAbility_Jump::UGasolineGameplayAbility_Jump(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
}

bool UGasolineGameplayAbility_Jump::CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const
{
	if (!ActorInfo || !ActorInfo->AvatarActor.IsValid())
	{
		return false;
	}

	const AGasolineCharacter* GasolineCharacter = Cast<AGasolineCharacter>(ActorInfo->AvatarActor.Get());
	if (!GasolineCharacter || !GasolineCharacter->CanJump())
	{
		return false;
	}

	if (!Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags))
	{
		return false;
	}

	return true;
}

void UGasolineGameplayAbility_Jump::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	// Stop jumping in case the ability blueprint doesn't call it.
	CharacterJumpStop();

	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UGasolineGameplayAbility_Jump::CharacterJumpStart()
{
	if (AGasolineCharacter* GasolineCharacter = GetGasolineCharacterFromActorInfo())
	{
		if (GasolineCharacter->IsLocallyControlled() && !GasolineCharacter->bPressedJump)
		{
			GasolineCharacter->UnCrouch();
			GasolineCharacter->Jump();
		}
	}
}

void UGasolineGameplayAbility_Jump::CharacterJumpStop()
{
	if (AGasolineCharacter* GasolineCharacter = GetGasolineCharacterFromActorInfo())
	{
		if (GasolineCharacter->IsLocallyControlled() && GasolineCharacter->bPressedJump)
		{
			GasolineCharacter->StopJumping();
		}
	}
}
