// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EGasolineAbilityInputID : uint8
{
	None,		// Necessary for GAS
	Confirm,	// Necessary for GAS
	Cancel,		// Necessary for GAS
	Jump,
	Dash
};
