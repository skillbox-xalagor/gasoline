// Fill out your copyright notice in the Description page of Project Settings.

#include "GasolineGameInstance.h"

#include "GameFramework/GameUserSettings.h"
#include "Kismet/GameplayStatics.h"

void UGasolineGameInstance::Init()
{
	Super::Init();

	GEngine->GameUserSettings->SetVSyncEnabled(true);
	GEngine->GameUserSettings->SetFullscreenMode(EWindowMode::WindowedFullscreen);
	GEngine->GameUserSettings->ApplySettings(true);
	GEngine->GameUserSettings->SaveSettings();
	GEngine->Exec(GetWorld(), TEXT("t.maxFPS 60"));
}
